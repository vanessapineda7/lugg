import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import { Font } from 'expo';

//Components
import Schedule from './components/schedule';
import Title from './components/title';
import Description from './components/description';
import ButtonPrimary from './components/button';


export default class App extends React.Component {

  constructor(props) {
    super(props);

    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
    
    this.state = {
      index: 0,
      routes: [
        { key: '1', title: 'Today', day: 'Sep 2nd' },
        { key: '2', title: 'Tomorrow', day: 'Sep 3rd' },
        { key: '3', title: 'Sunday', day: 'Sep 4th' },
        { key: '4', title: 'Monday', day: 'Sep 5th' },
        { key: '5', title: 'Tuesday', day: 'Sep 6th' },
      ],
      dataSource: [ {key: ''}, {key: 'Within the hour'}, {key: '6:00 PM - 7:00 PM'}, {key: '7:00 PM - 8:00 PM'}, {key: '8:00 PM - 9:00 PM'}, {key: '10:00 PM - 11:00 PM'}, {key: '11:00 PM - 12:00 AM'}, {key: '12:00 AM - 1:00 AM'}, {key: '1:00 AM - 2:00 AM'}, {key: '2:00 AM - 3:00 AM'} ],
      dataSource2: [ {key: ''}, {key: '6:00 PM - 7:00 PM'}, {key: '7:00 PM - 8:00 PM'}, {key: '8:00 PM - 9:00 PM'}, {key: '9:00 PM - 10:00 PM'}, {key: '10:00 PM - 11:00 PM'}, {key: '11:00 PM - 12:00 AM'}, {key: '12:00 AM - 1:00 AM'}, {key: '1:00 AM - 2:00 AM'}, {key: '2:00 AM - 3:00 AM'} ],
      fontLoaded: false 
    };
  }
  
  async componentDidMount() {
    await Font.loadAsync({
      'source-sans-pro-regular': require('./assets/fonts/SourceSansPro-Regular.ttf'),
      'source-sans-pro-bold': require('./assets/fonts/SourceSansPro-Bold.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  _handleIndexChange = index => this.setState({ index });

  _getLabelText = ({route}) => {
    return(
      <View>
        {this.state.fontLoaded ? (
          <View>
            <Text style={route == this.state.routes[this.state.index] ? styles.tabBarItemActive : styles.tabBarItem}>{route.title}</Text>
            <Text style={route == this.state.routes[this.state.index] ? styles.tabBarItemActiveDay : styles.tabBarItemDay}>{route.day}</Text> 
          </View>
        ) : null}
      </View>
    )
  }
  
  _renderHeader = props => <TabBar renderLabel={this._getLabelText} style={styles.tabBar} {...props} indicatorStyle={{backgroundColor: '#FDF982'}} scrollEnabled />;

  //Show the schedule
  GoSchedule1 = () => {
    return (
      <Schedule dataSource={this.state.dataSource} />
    )
  }
  GoSchedule2 = () => {
    return (
      <Schedule dataSource={this.state.dataSource2} />
    )
  }

  _renderScene = SceneMap({
    1: this.GoSchedule1,
    2: this.GoSchedule2,
    3: this.GoSchedule1,
    4: this.GoSchedule2,
    5: this.GoSchedule1,
  });

  render() {
    const initialLayout = {
      height: 0,
      width: Dimensions.get('window').width,
    };
    
    return (
      <View style={styles.container}>
        
        <Title />
        <Description />

        <View style={styles.line}></View>

        <View style={styles.calendar}>
          <TabViewAnimated
            style={styles.containerListView}
            navigationState={this.state}
            renderScene={this._renderScene}
            renderHeader={this._renderHeader}
            onIndexChange={this._handleIndexChange}
            initialLayout={initialLayout}
          />
        </View>

        <View>
          <ButtonPrimary />
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3D3A7C',
    paddingTop: 30,
  },
  line: {
    backgroundColor: '#858CB5',
    height: 1,
    marginLeft: 10,
    marginRight: 10,
  },
  calendar: {
    flex: 3,
    justifyContent: 'center',
  },
  containerListView: {
    flex: 1,
  },
  containerList: {
    flex: 1,
    backgroundColor: '#28285F',
  },
  tabBar: {
    backgroundColor: '#3D3A7C', 
  },
  tabBarItem: {
    color: 'white',
    fontSize: 16,
    justifyContent: 'center',
    fontFamily: 'source-sans-pro-bold',
  },
  tabBarItemDay: {
    color: '#858CB5',
    fontSize: 12,
    textAlign: 'center',
    fontFamily: 'source-sans-pro-regular',
  },
  tabBarItemActive: {
    color: '#FDF982',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'source-sans-pro-bold',
  },
  tabBarItemActiveDay: {
    color: '#FDF982',
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'source-sans-pro-regular',
  }
});
