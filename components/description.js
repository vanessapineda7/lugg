import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { Font } from 'expo';


export default class Description extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false 
    }
  }

  async componentDidMount() {
    await Font.loadAsync({
      'source-sans-pro-regular': require('../assets/fonts/SourceSansPro-Regular.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  render() {
  	return (
  		<View style={styles.description}>
        {this.state.fontLoaded ? (
          <View>
            <Text style={styles.text}>Set a time you would like us to arrive</Text>
            <Text style={styles.text}>at your pickup location</Text>
            <Text style={[styles.text, styles.white]}>Ikea Emerville</Text>
          </View>
        ) : null}
      </View>
  	)
  }
}


const styles = StyleSheet.create({
  description: {
    flex: 2,
    justifyContent: 'center',
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
    color: '#858CB5',
    fontFamily: 'source-sans-pro-regular',
  },
  white: {
    color: 'white',
  },
});

