import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { Font } from 'expo';


export default class Title extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fontLoaded: false	
		}
	}

	async componentDidMount() {
    await Font.loadAsync({
      'source-sans-pro-bold': require('../assets/fonts/SourceSansPro-Bold.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  render() {
		return (
			<View style={styles.title}>
				{this.state.fontLoaded ? (
		      <Text style={styles.textTitle}>
		        lugg
		      </Text>
		    ) : null}
	      <View style={styles.line}></View>
	    </View>
		)
	}
}


const styles = StyleSheet.create({
  title: {
    justifyContent: 'center',
  },
  textTitle: {
    color: 'white',
    fontSize: 22,
    paddingBottom: 10,
    textAlign: 'center',
    fontFamily: 'source-sans-pro-bold',
  },
  line: {
    backgroundColor: '#858CB5',
    height: 1,
    marginLeft: 10,
    marginRight: 10,
  },
});

