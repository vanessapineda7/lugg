import React from 'react'
import { StyleSheet, Text, View, Alert, TouchableOpacity } from 'react-native';
import { Font } from 'expo';


export default class ButtonPrimary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false 
    }
  }

  async componentDidMount() {
    await Font.loadAsync({
      'source-sans-pro-regular': require('../assets/fonts/SourceSansPro-Regular.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  render() {
  	return (
  		<TouchableOpacity onPress={() => {
        Alert.alert('You tapped the button!');
        }}
        style={styles.button}
      >
        {this.state.fontLoaded ? (
          <Text style={styles.buttonText}>Next</Text>
        ) : null}
      </TouchableOpacity>
  	)
  }
}


const styles = StyleSheet.create({
  button: {
    height: 55,
    backgroundColor: '#FDF982',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#3D3A7C',
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'source-sans-pro-regular',
  },
});

