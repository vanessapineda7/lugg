import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TouchableHighlight, Dimensions, ListView, FlatList } from 'react-native';


export default class Schedule extends React.Component {

  constructor(props) {
    super(props)
    this.state = { selected: 1 }
    this.viewabilityConfig = { itemVisiblePercentThreshold: 50}
  }

  handleViewableItemsChanged = (info) => {
    this.setState({
	  	selected: info["viewableItems"][1]["index"]
		})
  }

	render() {
		return (
	    <FlatList
				onViewableItemsChanged={this.handleViewableItemsChanged}
			  viewabilityConfig={this.viewabilityConfig}
	    	onScroll={e=>true}
	    	style={styles.containerList}
			  data={this.props.dataSource}
			  renderItem={({item, index}) => (
			    <TouchableHighlight>
			      <Text style={this.state.selected == index ? [styles.listData, styles.selected] : styles.listData}>{item.key}</Text>
			    </TouchableHighlight>
			  )}
			/>
	  )
	}
}

const styles = StyleSheet.create({
  containerList: {
    flex: 1,
    backgroundColor: '#28285F',
  },
  listData: {
    paddingTop: 9,
    paddingBottom: 9,
    marginBottom: 7,
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
  },
  selected: {
  	color: '#28285F',
  	backgroundColor: '#FDF982'
  }
});